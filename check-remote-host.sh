#!/bin/bash

host=""
port=80
timeout=30  # Adjust the timeout value as needed

if nc -w $timeout -z "$host" "$port"; then
    echo "Host is available on port $port."
else
    echo "Host is not available on port $port. Sending notification..."
    python send_failure_notification.py
fi

