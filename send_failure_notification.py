import requests
from log import get_logger

API_TOKEN = ""
TELEGRAM_CHAT_ID = ""

logger = get_logger(__name__)

def send_to_telegram(message, is_image=False, is_video=False):
    """
    Sends a message to a chat with id TELEGRAM_CHAT_ID. The id can be determined using https://t.me/RawDataBot
    A more advanced communication can be established using https://docs.python-telegram-bot.org/en/stable/examples.conversationbot.html#conversationbot-diagram

    Returns True if executed successfully, False otherwise. True doesn't guarantee though that API
    has delivered the message.

    May also send other things:
    - https://core.telegram.org/bots/api#sendvideo
    - https://core.telegram.org/bots/api#sendaudio
    - https://core.telegram.org/bots/api#senddocument
    """
    try:
        apiURL = f"https://api.telegram.org/bot{API_TOKEN}/sendMessage"
        response = requests.post(apiURL, json={'chat_id': TELEGRAM_CHAT_ID, 'text': message})

        logger.info(f"Message has been sent: {message}")
        logger.debug(response.text)
        return True
    except Exception as e:
        logger.error("Unhandled exception while sending Telegram message:")
        logger.error(e)

send_to_telegram("Warning: the host is unavailable.")
