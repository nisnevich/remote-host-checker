# remote-host-checker

A simple tool to check remote host availability, and send a Telegram notification whenever the host is unreachable.

## Getting started

There are few things to make it work:

### Create a Telegram bot.

Message @BotFather on Telegram to register your bot and receive its authentication token.

### Find your chat ID.

Message @RawDataBot on Telegram - it will send you JSON data where you will see your chat ID.

### Add the bot API token and chat ID to send_failure_notification.py

Set 2 variables - API_TOKEN and TELEGRAM_CHAT_ID.

### Add host info to check-remote-host.sh

Set 2 more variables - host (public address) and port.

### (optional) Add a cron job for check-remote-host.sh.

Or whichever way you prefer to call it. Congrats, now you will know whenever your host fails. Cheers!
