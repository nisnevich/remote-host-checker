import os
import sys
import logging
import platform
from logging.handlers import RotatingFileHandler

if platform.system() == 'Windows':
    # Get path relative to current file; dirname is used 3 times to get project root
    PROJECT_ROOT = os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
    FILE_SYSTEM_ROOT = "C:\\"
elif platform.system() == 'Linux':
    PROJECT_ROOT = os.getcwd()
    FILE_SYSTEM_ROOT = "/"
else:
    print('This system is not supported')
    exit()

LOG_PATH = os.path.join(PROJECT_ROOT, "logs", "remote-host-checker.log")
LOG_FILE_SIZE = 1024 * 1024 * 10
DEFAULT_LOG_LEVEL = logging.DEBUG


def get_logger(module_name):
    logger = logging.getLogger(module_name)
    logger.setLevel(DEFAULT_LOG_LEVEL)
    logger.addHandler(log_handler)
    logger.addHandler(console_handler)
    return logger


if not os.path.exists(LOG_PATH):
    os.makedirs(os.path.dirname(LOG_PATH), exist_ok=True)

log_handler = RotatingFileHandler(LOG_PATH, mode='a', maxBytes=LOG_FILE_SIZE, backupCount=5, encoding='utf-8')
log_handler.setLevel(DEFAULT_LOG_LEVEL)

console_handler = logging.StreamHandler(stream=sys.stdout)
console_handler.setLevel(DEFAULT_LOG_LEVEL)

# Set the log level and format
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
console_handler.setFormatter(formatter)
log_handler.setFormatter(formatter)
